# k8s Maildrop Web

Maildrop Website

Forked from gitlab.com/markbeeson/maildrop/

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name      |    Description            | Default                          |
| :-------------------- | ------------------------- | -------------------------------- |
|  `MAILDROP_API_HOST`  | m242/Maildrop API host    | `api-maildrop.$MAILDROP_DOMAIN`  |
|  `MAILDROP_API_KEY`   | m242/Maildrop API key     | undef                            |
|  `MAILDROP_DOMAIN`    | m242/Maildrop domain      | `demo.local`                     |
