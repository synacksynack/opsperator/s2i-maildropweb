IMAGE = opsperator/maildropweb

.PHONY: install
install:
	@@NODE_ENV=build npm install --unsafe-perm
	@@npm install -g typescript

.PHONY: prep-runtime
prep-runtime:
	@@if test -z "$$MAILDROP_DOMAIN"; then \
	    MAILDROP_DOMAIN=demo.local; \
	fi; \
	grep -R maildrop.cc src | awk -F: '{print $$1}' | \
	    while read f; \
	    do \
		sed -i "s|maildrop.cc|$$MAILDROP_DOMAIN|g" $$f; \
	    done; \
	if test -z "$$MAILDROP_API_HOST"; then \
	    MAILDROP_API_HOST=api-maildrop.$$MAILDROP_DOMAIN; \
	fi; \
	REACT_APP_API_HOST=https://$$MAILDROP_API_HOST \
	    REACT_APP_API_KEY=$$MAILDROP_API_KEY \
	    ./node_modules/.bin/react-scripts build

.PHONY: run
run: prep-runtime
	@@./node_modules/.bin/serve -s build

.PHONY: build
	docker build -t $(IMAGE) -f Dockerfile.debug-s2i .

.PHONY: dockertest
dockertest: build
	docker run -p5000:5000 -e MAILDROP_DOMAIN=demo.local \
	    --entrypoint /usr/libexec/s2i/run -it $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service configmap statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: test
test:
	@@echo no tests
